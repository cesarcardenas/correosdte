<?php

/* 
 * Archivo para llenar los datos en la base de dato
 * la tabla en la base de datos se llama correos
 * el archivo debe llamarse empresas.csv y debe estar en la misma carpeta que este archivo.
 * Autor: Cesar Cardenas (cesarcardenas@puntotron.com)
 */
// conexión
if(!$mysqli = new mysqli(localhost, 'AQUITUSDATOS', 'AQUITUSDATOS', 'AQUITUSDATOS')){
    echo "Error ";
}
if ($mysqli->connect_errno) {
    printf("Falló la conexión: %s\n", $mysqli->connect_error);
    exit();
}
$cont=0;

$mysqli->query("truncate table correos;");
echo "Se limpia la tabla </br>";
 $q = "LOAD DATA LOCAL INFILE './empresas.csv'
INTO TABLE correos 
CHARACTER SET latin1
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '\"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(rut, razon, resolucion, fresolucion, correo, url) ";
 if(!$resultado=$mysqli->query($q)){
            echo "Falló la creación de la tabla: (" . $mysqli->errno . ") " . $mysqli->error ."</br>";
            
        }
   $mysqli->close();
   echo "importacion correcta </br>";
   print_r($resultado);
   