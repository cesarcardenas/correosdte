<?php
/*Conuslta por rut para obteenr el dte email
devuelve un json con los datos asociados al rut
Version 0.3 
Autor: Cesar Cardenas (cesarcardenas@puntotron.com) */
$datos = array();

function limpiarCadena($valor) {
    $valor = preg_replace(array('/(\d{7,8})/','/\D/'),array('\1',''), $valor);
    return $valor;
}

if (isset($_GET) && $_GET['rut'] != "") {
    $rut = limpiarCadena(filter_input(INPUT_GET, 'rut', FILTER_SANITIZE_SPECIAL_CHARS));
    $rut = substr($rut, 0,8);
    if (!$mysqli = new mysqli(localhost, 'AQUITUSDATOS', 'AQUITUSDATOS', 'AQUITUSDATOS')) {
        echo json_encode($datos, JSON_FORCE_OBJECT);
    }
    if ($mysqli->connect_errno) {
        echo json_encode($datos, JSON_FORCE_OBJECT);
        exit();
    }
    $q = "SELECT * FROM `correos` WHERE `rut` LIKE '%$rut%'";
    $resultado = $mysqli->query($q);
    if ($resultado->num_rows === 0) {
        echo json_encode($datos, JSON_FORCE_OBJECT);
        exit;
    }
    if ($resultado->num_rows === 1) {
        $datos = $resultado->fetch_assoc();
        echo json_encode($datos);
    } else {
        echo json_encode($datos, JSON_FORCE_OBJECT);
    }
} else {
    echo json_encode($datos, JSON_FORCE_OBJECT);
}
